var soundMap = {
	crumble3: ['assets/sound/crumble3.mp3', 'assets/sound/crumble3.ogg'],
	crumble4: ['assets/sound/crumble4.mp3', 'assets/sound/crumble4.ogg'],
	jump0: ['assets/sound/jump0.mp3', 'assets/sound/jump0.ogg'],
	jump2: ['assets/sound/jump2.mp3', 'assets/sound/jump2.ogg'],
	crumble1: ['assets/sound/crumble1.mp3', 'assets/sound/crumble1.ogg'],
	crumble2: ['assets/sound/crumble2.mp3', 'assets/sound/crumble2.ogg'],
	crumble5: ['assets/sound/crumble5.mp3', 'assets/sound/crumble5.ogg'],
	jump1: ['assets/sound/jump1.mp3', 'assets/sound/jump1.ogg'],
	gem: ['assets/sound/gem.mp3', 'assets/sound/gem.ogg'],
	crumble0: ['assets/sound/crumble0.mp3', 'assets/sound/crumble0.ogg'],
	autsj0: ['assets/sound/autsj0.mp3', 'assets/sound/autsj0.ogg'],
};