#!/usr/bin/python

import os
import re

indexFile = open('index.html', 'w')


header = """<!DOCTYPE html>
<html>
    <head>

   
    <title>Ruins & Riches</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">

    <meta property="og:title" content="Ruins & Riches"/>
    <meta property="og:image" content="https://jellenauta.com/games/Alakajam/ruins-and-riches/scrshot.png"/>
    <meta property="og:site_name" content="Games by Jelle & Anneroos"/>
    <meta property="og:description" content="Everyone knows that ancient ruins are filled with ancient treasures, just waiting for enterprising adventurers such as yourself. To clear a path through the ruins you have to smash them up a bit, just try to take it easy so UNESCO won't get mad."/>
    <meta property="og:url" content="https://jellenauta.com/games/Alakajam/ruins-and-riches/"/>
    
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favoicon.png">  

        <link rel="stylesheet" type="text/css" href="stylesheet.css" />
        <!-- font -->

    <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>	
    <!--   <meta name="viewport"   content="width=device-width, user-scalable=no, initial-scale=1.0" /> -->
    <script src="lib/crafty.js"></script>
    <script src="src/params.js"></script>"""
footer = """    <script>
      window.addEventListener('load', Game.start);
    </script>
    
    <!--  Thumbnail voor Facebook enzo. ;) -->
    <link rel="image_src" href="assets/images/scrshot.png"/>  
  </head>
  <body>
        <!--Add your own HTML!-->   
        
    <div id="canvascontainer">  
        
      
      <div id="cr-stage">
       
      </div>
    </div>
   
    <div>
    
        <p><br>Made by <a href="http://anneroos.home.fmf.nl/">Anneroos</a> and <a href="https://jellenauta.com/">Jelle</a> for <a href="https://alakajam.com/11th-alakajam/1104/ruins-riches/">Alakajam 11</a> with the theme 'Ancient ruins'.</p> 
        
      
    </div>
    
  </body>
</html>"""

try:
  jsRegex = re.compile('.*\.js');
  indexFile.write(header)
  for root, dirs, files in os.walk('./src/'):
    for name in files:
      if name != "params.js":
        indexFile.write('    <script src="');
        indexFile.write(os.path.join(root, name));
        indexFile.write('"></script>\n');
  for root, dirs, files in os.walk('./assets/'):
    for name in files:
      if jsRegex.match(name):
        indexFile.write('    <script src="');
        indexFile.write(os.path.join(root, name));
        indexFile.write('"></script>\n');

  indexFile.write(footer)

finally:
  indexFile.close()