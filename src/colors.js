
mycolors = {
   
    background : '#425063',
    backgroundStart : '#425063',

    title: '#ffffff',
    next: '#ccffff',

    scoreText : "#ffffff",	 

    infoText: '#000000',
    fadeScreen: "#342500",
    
    buttonUnselected: '#ffffff',
    buttonSelected: '#ffffff',
    selectedText: '#ff0000',
    unselectedText: '#000000',
}