// let overlay;
// let scoreUI;
// let musicMute = false;
let gameIsPaused = false;
let blocks = [];
let player;
let resetting = false;
let currentLevel;
let stonesCrushed = 0;
let stonesCrushedThisLevel = 0;

// MAIN SCENE
Crafty.scene('Main', function() {	
	stonesCrushed = 0;
	stonesCrushedThisLevel = 0;

	// CREATE EXTRA LAYER FOR UI ELEMENTS
    Crafty.createLayer("UI", "DOM", {
      xResponse: 0, yResponse:0, scaleResponse:0, z: 50
    });
    // BACKGROUND
	Crafty.background(mycolors.background +' url(assets/images/bgLevel.png) repeat center center'); 
	
	Crafty.timer.FPS(60);
	Crafty.audio.setChannels(12);

	let levelIndex = 0; 		

	// FUNCTION FOR UPDATING HIGHSCORES - IF ANY ;) - scoreUI DOESN'T EVEN EXIST YET
	let updateHighScore = function(){				
		var newState = {highscores_alakajam_11: scoreUI.highscores};			
		saveState(newState);
	};	
	Crafty.bind('UpdateHighscore',updateHighScore);

	var endOfLevel = function(){
		Crafty('Explanation').each(function(){
			this.destroy();
		});
		Crafty.viewport.zoom(2, 
			Math.min(Math.max(player.x,(Game.width+2*params.sqSize)/4),(Game.width + 2*params.sqSize)*3/4), 
			Math.min(Math.max(player.y,(Game.height+2*params.sqSize)/4),(Game.height+2*params.sqSize)*3/4), 
			1000);
		scoreUI.fadeOut(1000);
		scoreUI.delay(function(){
			if(currentLevel == "endOfGame"){
				Crafty.scene("Main");
			} else {
				Crafty.trigger("nextLevel")}
			}
		,1000);
	};

	var buildGame = function(){		
		scrollY = 0;	
		// and unbind everything from Crafty
		Crafty.unbind('EnterFrame');
		Crafty.unbind('die');
		Crafty.unbind('reset');
		Crafty.unbind('nextLevel');
		Crafty.unbind('EndOfLevel');
		Crafty.unbind('crushStone');

		// bind everything
		Crafty.bind("die", () => {
			resetting = true;
		});
		Crafty.bind("reset", () => {
			resetting = true;
		});

		Crafty.bind("EndOfLevel",endOfLevel);

		Crafty.bind("nextLevel", () => {
			stonesCrushed += stonesCrushedThisLevel;
			stonesCrushedThisLevel = 0;
			levelIndex += 1;
			resetting = true;
		});
		Crafty.bind("crushStone", () => {
			stonesCrushedThisLevel++;
		})

		resetLevel();

		// LET'S KEEP TIME 
		let time = 0;

		// TIME TO MOVE THINGS
		Crafty.bind('EnterFrame', function(e){
			if(!gameIsPaused){
				let dt = e.dt;
				time += dt;
				// MOVE THE PLAYER
				player.updatePos(dt);
				for (var bl = 0; bl < blocks.length; bl++) {
					blocks[bl].updatePos(dt);
				}	

				// SCROLL THE BACKGROUND
				// background.updatePos(dt, scrollY);				
				if (resetting) {
					resetLevel();
				}
			}
		});
	};

	let getNeighbors = (coord, cols, rows) => {
		let neighs = []
		if (coord[0] >= 1) {
			neighs.push([coord[0] - 1, coord[1]]);
		}
		if (coord[0] + 1 < cols) {
			neighs.push([coord[0] + 1, coord[1]]);
		}
		if (coord[1] >= 1) {
			neighs.push([coord[0], coord[1] - 1]);
		}
		if (coord[1] + 1 < rows) {
			neighs.push([coord[0], coord[1] + 1]);
		}
		return neighs;
	}

	var stampOutShape = (data, col, row) => {
		let shape = [];
		let tileIdx = data[row][col];
		if (tileIdx == 0) {
			console.log("WARNING: trying to stamp out zero-shape!");
			return [];
		}
		let queue = [[col, row]];
		data[row][col] = 0;
		while (queue.length > 0) {
			let sq = queue.shift();
			shape.push([sq[0] - col, sq[1] - row]);
			let neighs = getNeighbors(sq, data[0].length, data.length);
			for (let idx = 0; idx < neighs.length; idx++) {
				let n = neighs[idx];
				if (data[n[1]][n[0]] == tileIdx) {
					queue.push(n);
					data[n[1]][n[0]] = 0;
				}
			}
		}
		return shape;
	}

	let matrixFromData = (data, w, h) => {
		let matrix = [];
		for (let row = 0; row < h; ++row) {
			matrix.push([]);
			for (let col = 0; col < w; ++col) {
				let tileIdx = data[w * row + col];
				matrix[row].push(tileIdx);
			}
		}
		return matrix;
	}

	let resetLevel = () => {
		// DESTROY any remains from previous levels		
		Crafty.viewport.scale(1);
		Crafty('2D').each(function(){
			this.destroy();
		});
		blocks = [];
		gameIsPaused = false;
		blocks = [];		
		player = null;
		stonesCrushedThisLevel = 0;
		currentLevel = levels[levelIndex];
		let level = TileMaps[currentLevel];
		let gemsInLevel = 0;
		for (let idx = level.layers.length - 1; idx >= 0; idx--) {
			let layer = level.layers[idx];
			let matrix = matrixFromData(layer.data, level.width, level.height);
			for (let col = 0; col < level.width; ++col) {
				for (let row = 0; row < level.height; ++row) {
					let tileIdx = matrix[row][col];
					if (layer.name === "blocks") {
						if (tileIdx >= 5) {
							// Flood fill to get entire block
							let shape = stampOutShape(matrix, col, row);
							let newBlock = Crafty.e('2D, Canvas, Block').attr({
								z: params.zLevels['blocks'],
								tileIdx: tileIdx
							})._Block(col, row, shape, tileIdx);
							blocks.push(newBlock);
						}
						if (tileIdx == 1) {
							player = Crafty.e("Player")._Player(params.sqSize * col, params.sqSize * row);
						}
					} else if (layer.name === "gems") {
						if (tileIdx == 2) {
							gemsInLevel++;
							Crafty.e("Gem").attr({x: params.sqSize * col, y: params.sqSize * row});
						}
					}
				}
			}
		}
		player.gemsGoal = gemsInLevel;
		resetting = false;
		// A UI ENTITY THAT LIVES IN THE UI LAYER
 		scoreUI = Crafty.e('ScoreUI')._ScoreUI(levels.length-1);
 		scoreUI.updateGemScore(player.gems, player.gemsGoal);
 		
		// FADE IN		
 		scoreUI.fadeIn(1200);

 		Crafty.viewport.x = -params.sqSize;
		Crafty.viewport.y = -params.sqSize;

		if ( currentLevel in explanations){
			expls = explanations[currentLevel];
			for ( var i = 0; i<  expls.length; i++){
				let expl = expls[i];
				Crafty.e('2D,UI,Text,Tween, Explanation')		
				.attr({x:expl.x, y:expl.y, w:500, h:200, alpha:1,z:params.zLevels["explanations"]}).textColor(mycolors.infoText)		
				.textFont({ family: fontFamily2,size: '30px'}).textAlign('left')
				.text(expl.text);
			}
		}
		if (currentLevel == "endOfGame"){
			Crafty.e('2D,UI,Text,Tween, Explanation')		
				.attr({x:0, y:60, w:Game.width+2*params.sqSize, h:200, alpha:1,z:params.zLevels["explanations"]}).textColor(mycolors.infoText)		
				.textFont({ family: fontFamily2,size: '40px'}).textAlign('center')
				.text("Congratulations, you found all the gems!");
			Crafty.e('2D,UI,Text,Tween, Explanation')		
				.attr({x:0, y:160, w:Game.width+2*params.sqSize, h:200, alpha:1,z:params.zLevels["explanations"]}).textColor(mycolors.infoText)		
				.textFont({ family: fontFamily2,size: '30px'}).textAlign('center')
				.text("You demolished " 
					+ stonesCrushed  + " stones. <br> Perhaps you can be more careful next time. :)"
					+ "<br><br> Collect the last gem to start again from the beginning.");
		}
	}
	
	buildGame();		
});