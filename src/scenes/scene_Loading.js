// LOADING SCENE
Crafty.scene('Loading', function() {
	
	document.documentElement.addEventListener('keydown', function (e) {
	    if ( ( e.keycode || e.which ) == 32 || ( e.keycode || e.which ) == 38 || ( e.keycode || e.which ) == 40) {
	        e.preventDefault();
	    }
	}, false);
	
	Crafty.e('2D, DOM, Text')
		.attr({
			x: 0,
			y: 370,
			w: Game.width,
			h: 300,
			z: 8
		})
		.text('Loading...')
		.textFont({
			size: '34px',
			family: fontFamily2
		})
		.textAlign('center')
		.css({
			'text-align': 'center',			
			'color': '#000000'			
		});
	Crafty.load(assetsObject,
		function() {
			//when loaded	
					
			Crafty.sprite(35, 35, "assets/images/player.png", {
				player: [0,0],				
			});	

			Crafty.sprite(20, 20, "assets/images/tiles2.png", {
				tile2: [0,0],	
			});

			Crafty.sprite(20, 20, "assets/images/highlight.png", {
				highlight: [0,0],	
			});


			Crafty.sprite(40, 40, "assets/images/shade.png", {
				shade: [0,0],	
			});

			Crafty.sprite(40, 40, "assets/images/gem.png", {
				gem: [0,0],	
			});

			Crafty.sprite(1280, 960, "assets/images/oldLook.png", {
				oldLook: [0,0],	
			});


			// I wonder if it works
			musicMuter = Crafty.e("Keyboard").bind("KeyDown",function(keyEvent){			
				if (keyEvent.key === Crafty.keys.M ) {
					if(musicMute){
						musicMute = false;
						Crafty.audio.unmute();
					} else {
						musicMute = true;
						Crafty.audio.mute();
					}
				} 
			
			});
			Crafty.scene('StartScreen');
			// Crafty.scene('Main');
			
		},

		function(e) { // onProgress
			// console.log(e.src);
		},

		function(e) {
			console.log('loading error:');
			console.log(e.src);
			console.log(e);
		}
	);
});