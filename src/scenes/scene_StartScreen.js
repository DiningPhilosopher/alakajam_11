Crafty.scene('StartScreen', function() {
   	// Crafty.background('url(assets/images/background.png)');	
   	Crafty.background(mycolors.backgroundStart);// + ' url(assets/images/bgSite.png) repeat center center'); 
 	Crafty.background(mycolors.background +' url(assets/images/backgroundStart.jpg) repeat center center'); 
   	// FAKE PLAYER TO SPICE UP THE START SCREEN
 	// player = Crafty.e("Player")._Player(335,100);
	// player.startScreenVersion = true;
	// Crafty.bind("EnterFrame",function(e){player.updatePos(e.dt)});
	
	title = Crafty.e('2D,DOM,Tween, Mouse, Delay,Keyboard, Text')
		.attr({
			x: -20,
			y: 200,
			w: Game.width,
			h: 200,
			z: 820
		})
		.text('Ruins & Riches')
		.textFont({
			size: '160px',
			family: fontFamily2
		})
		.textAlign('center')
		.css({
			'text-align': 'center',			
			'color': '#342500'	
		});


	// time = 0;
	// Crafty.bind('EnterFrame',function(e){
	// 	dt = e.dt;
	// 	time += dt;
	// 	title.y = 200+50*Math.sin((time+300)/1000);
		
	// });


	startButton = Crafty.e('2D,DOM,Tween, Mouse, Delay,Keyboard, Text')
		.attr({
			x: 0,
			y: 760,
			w: Game.width,
			h: 60,
			z: 810
		})
		.text('Press Space to start')
		.textFont({
			size: '28px',
			family: fontFamily2
		})
		.textAlign('center')
		.css({
			'text-align': 'center',			
			'color': '#342500'	,
			'cursor':'pointer'		
		});

	startGame = function(){
		// Crafty('2D').each(function(){
		// 	this.tween({alpha:0},500);
		// });
		fadeScreen = Crafty.e("2D,DOM,Color,Tween").attr({w:Game.width, h:Game.height,z:1020,alpha:0}).color(mycolors.fadeScreen);
		fadeScreen.tween({alpha:1},500);
	
		startButton.delay(function(){
			Crafty.scene('Main');
			
		},550);
	};	
	
	startButton.uniqueBind('Click',function(keyEvent){
		startGame();		
	}); 
	startButton.uniqueBind('KeyDown',function(keyEvent){
		if (keyEvent.key === Crafty.keys.SPACE ) {
			startGame();
		}

	}); 		

    

});
