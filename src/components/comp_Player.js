Crafty.c("Player", {
	init: function() {
		this.requires("2D, Canvas, player, Collision, Delay, Tween, SpriteAnimation"); 		
		this.origin('center');
		this.centerX = Math.floor(this.ox);
		this.collision([
			this.centerX - 10,0,
			this.centerX + 10,0,
			this.centerX + 10,35,
			this.centerX - 10,35
		]);
		this.playable = true;
		this.state = "airborne";
		// speed	
		this.vx = 0;		
		this.vy = 0;
		// DO NOT TOUCH THESE, THEY ARE SPECIFIC TO THE PUZZLES
		// Possible jumps: (5, 1), (4, 2)
		this.ax = 0.004;
		this.frictionX = 0.8; // 0.75
		this.jumpAcc = 0.11;
		this.jumpV = 0.33;
		this.maxvx = 0.3;
		this.maxvy = 0.6;
		this.fallDrag = 0.96;
		this.g = 0.003;
		// Overwrite parameters below for experimentation.


		// time that you can jump
		this.jumpCounter = 0;
		this.maxJumpCounter = 7; //15
		this.airborneCounter = 0;
		this.maxAirborneToJump = 3;

		this.hasWon = false;
		// this.timecounter = 0;

		this.goingRight = false;
		this.goingLeft = false;
		this.jumpKey = false;
		this.downKey = false;

		this.myAxe = Crafty.e('Axe')._Axe();
		this.attach(this.myAxe);

		this.gems = 0;
	},
	
	_Player : function(x,y){ 		
		this.x = x;
		this.y = y;
		this.z = params.zLevels["player"];
		this.bind('KeyDown', this.keyDown);
		this.bind('KeyUp', this.keyUp);

		// REELS
		this.reel('Stand', 500, 0,0,1);
		this.reel('PlayerWalking', 500, 1,0,3);
		this.reel('Jump', 1000, [[4, 0]]);
		this.reel('Fall', 1000, [[5, 0]]);
	
		return this;
	},

	setPlayerStart: function(x,y){
		this.startX = x;
		this.startY = y;
	},
	
	gameOver: function(){	
		this.playable = false;		
	},

	updateVelocity: function(dt){
		// update vx
		if(!this.goingLeft && !this.goingRight){
			this.vx = this.vx * this.frictionX;			
		}
		if (this.goingLeft){
			this.vx -= this.ax*dt;			
		}
		if (this.goingRight){
			this.vx += this.ax*dt;					
		}
		// state- and vy updates
		this.vy += this.g * dt;
		if(this.state == "floor"){			
			if (this.jumpKey){
				this.jumpCounter = 0;
				this.vy = -this.jumpV;
				this.state = "jump";
				Crafty.audio.play("jump"+Math.floor(Math.random()*3),0.4,1);			
			}
		} else if(this.state == "jump"){	
			this.jumpCounter += 1;
			// update vy
			this.vy -= (1 - this.jumpCounter/ (2*this.maxJumpCounter)) * this.jumpAcc;
			if (!this.jumpKey || this.jumpCounter >= this.maxJumpCounter){
				this.state = "airborne";
			}
		}

		// constrain velocities
		this.vx = Math.max(-this.maxvx, Math.min(this.maxvx, this.vx));
		this.vy = Math.max(-this.maxvy, this.vy);
		if (this.vy > this.maxvy) {
			this.vy *= this.fallDrag;
		}
	},

	updatePos: function(dt){
		if(this.playable){
			this.updateVelocity(dt);
			// update positions
			let oldx = this.x;
			let oldy = this.y;
			this.newy = this.y + this.vy*dt;			
			this.newx = this.x + this.vx*dt;

			// collect gems
			let gemHits = this.hit('Gem');
			if (gemHits != null) {
				for (let idx = 0; idx < gemHits.length; ++idx) {
					gemHits[idx].obj.collect();
					this.gems++;
					Crafty.audio.play("gem",0.4,1);
					scoreUI.updateGemScore(this.gems, this.gemsGoal);
					if (this.gems == this.gemsGoal) {
						this.playable = false;
						Crafty.trigger("EndOfLevel");
						
					}
				}
			} 

			// CHECK VERTICAL COLLISION
			
			// First: if we are already colliding with a block, we got hit by it and die.
			let hitDatas = this.hit('Obstacle')
			if (hitDatas != null) {
				Crafty.audio.play("autsj0",0.4,1);
				Crafty.trigger("die");
			}
			this.y = this.newy;
			hitDatas = this.hit('Obstacle')
			if (hitDatas != null) {
				// First check whether we touch two blocks - in that case we die.
				this.y = oldy;
				if (this.vy > 0) {
					// hit a block from above after y-movement
					if(this.state != "floor"){
						Crafty.audio.play("land0");
						this.state = "floor";
						this.jumpKey = false;
					}
 	            } else if (this.state == "jump"){
					// hit the ceiling
					this.jumpCounter = this.maxJumpCounter;
					this.state = "airborne";
					// if the ceiling was falling, adopt speed of fastest-falling ceiling.
					// let maxVy = 0.0;
					// for (let idx = 0; idx < hitDatas.length; ++idx) {
					// 	let hitObj = hitDatas[0].obj;
					// 	if (hitObj.vy > maxVy) {
					// 		maxVy = hitObj.vy;
					// 	}
					// }
					// this.vy = maxVy;
				}
				this.vy = 0
	        } else {
				if (this.state == "floor") {
					// We were on a floor, but walked off.
					this.state = "airborne";
				}
			}

	        // CHECK HORIZONTAL POSITION
			this.x = this.newx;
			// "Collide" near edge of screen or when bumping into a block.
			hitDatas = this.hit('Obstacle');
			if (hitDatas != null || this.x <= 0 ||  this.x + this.w >= Game.width + 2 * params.sqSize) {
				this.x = oldx;
				this.vx = 0;
			}

			// Flip picture?
			if(this.vx < 0){
				this.flip("X");
			} else if(this.vx > 0){
				this.unflip("X");
			}

			// ANIMATIONS
			if(this.state == "floor"){
				if(Math.abs(this.vx) <0.1 && !this.isPlaying('Stand')){
					this.animate('Stand',-1);				
				} else if(Math.abs(this.vx) >0.1 && !this.isPlaying('PlayerWalking')){
					this.animate('PlayerWalking',-1);
				}
			} else if ( (this.state =="airborne" || this.state =="jump" )&& !this.isPlaying("Fall") && this.vy >0 ) {
				this.animate('Fall',-1);	
			} else if ( (this.state =="airborne" || this.state =="jump" )&& !this.isPlaying("Jump") && this.vy < 0 ) {
				this.animate('Jump',-1);	
			}
			let dir = 1;
			if (this._flipX) {
				dir = -1;
			}
			this.myAxe.updatePos(dir);
	    }
	},

	disappear: function(){
		this.hittable = false;
		this.tween({alpha:0.3},300);
		this.delay(function(){
			this.tween({alpha:1},300);
		},300,0);
		this.delay(function(){
			this.hittable = true;
		},600,0);	
	},

	keyDown : function(keyEvent){	
		if(this.playable){
			if (keyEvent.key === Crafty.keys.RIGHT_ARROW) {
				this.goingRight = true;				
			} else if (keyEvent.key === Crafty.keys.LEFT_ARROW) {
				this.goingLeft=true;
			} else if (keyEvent.key === Crafty.keys.UP_ARROW) {
				this.jumpKey = true;				
			} else if (keyEvent.key === Crafty.keys.DOWN_ARROW) {
				this.downKey = true;
			}  else if (keyEvent.key === Crafty.keys.SPACE) {
				// enable chopping until we chop a block.
				this.myAxe.chop();
			} else if (keyEvent.key === Crafty.keys.R) {
				Crafty.trigger("reset");
			}
		}
		if (keyEvent.key === Crafty.keys.R) {
			// bgMusic.fadeMusic(30,20);						
			// Crafty.trigger('UpdateHighscore');
			// resetLevel();
		} else if (keyEvent.key === Crafty.keys.P) {
			Crafty.pause();
		}
	},

	keyUp : function(keyEvent){
		if(this.playable){
			if (keyEvent.key === Crafty.keys.RIGHT_ARROW ) {
				this.goingRight = false;
			} else if (keyEvent.key === Crafty.keys.LEFT_ARROW  ) {
				this.goingLeft = false;
			} else if (keyEvent.key === Crafty.keys.UP_ARROW  ) {
				this.jumpKey = false;
				this.jumpCounter = this.maxJumpCounter;
			} else if (keyEvent.key === Crafty.keys.DOWN_ARROW) {				
				this.downKey = false;
			} else if (keyEvent.key === Crafty.keys.SPACE) {
				this.myAxe.stopChop();
			}
		}			
	},	
});


