 Crafty.c("BgScroller", {
    init: function() {
        /* Create an Entity for every image.
         * The "repeat" is essential here as the Entity's width is 3x the canvas width (which equals
         * the width of the original image).
         */
			this.bgWidth = 1200;
			this.bgHeight = 1200;
            this.mgWidth = 1200;
            this.mgHeight = 1600;
            this.fgWidth = 1200;
            this.fgHeight = 1600;
         var IMG_GROUND = "../ld47/assets/images/fg.png",
	        IMG_VEG = "../ld47/assets/images/mg.png",
	        IMG_SKY = "../ld47/assets/images/bg.png"
        this._bgImage = Crafty.e("2D, Canvas, Image").image(IMG_SKY, "repeat")
                                .attr({z:0, x:0, y:0, w: this.bgWidth, h:this.bgHeight*3});
        this._mgImage = Crafty.e("2D, Canvas, Image").image(IMG_VEG, "repeat")
                                .attr({z:10, x:0, y:0, w: this.mgWidth , h:this.mgHeight*3});
        this._fgImage = Crafty.e("2D, Canvas, Image").image(IMG_GROUND, "repeat")
                                .attr({z:20, x:0, y:0, w: this.fgWidth , h:this.fgHeight*3});
                                
       this.bgSpeed = 6/16;
       this.mgSpeed = 8/16;
       this.fgSpeed = -5/16;
        
    },

    updatePos: function(dt, scrollY){
        // this._bgImage.y += this.bgSpeed*dt;
        // this._mgImage.y += this.mgSpeed*dt;
        // this._mgImage.y += this.fgSpeed*dt;

        tempBgY = ((scrollY*(1-this.bgSpeed) % this.bgHeight) + this.bgHeight) % this.bgHeight; 
        this._bgImage.y = - (Math.floor((scrollY)/this.bgHeight) +1)*this.bgHeight - tempBgY;
        
        tempMgY = ((scrollY*(1-this.mgSpeed) % this.mgHeight) + this.mgHeight) % this.mgHeight;
        this._mgImage.y = - (Math.floor((scrollY)/this.mgHeight) +1)*this.mgHeight - tempMgY;
        
        tempFgY = ((scrollY*(1-this.fgSpeed) % this.fgHeight) + this.fgHeight) % this.fgHeight; 
        this._fgImage.y = - (Math.floor((scrollY)/this.fgHeight) +1)*this.fgHeight - tempFgY;
    }
});
