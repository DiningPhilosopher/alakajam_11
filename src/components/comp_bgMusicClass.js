Crafty.c("bgMusicClass", {

	init: function() {
		this.timeCounter = 0;
		this.timeMax = 1000;
		this.maxVolume = 1;		
		this.state = 'preloop';
		this.bind('EnterFrame',this.everyFrame);
		this.requires('Keyboard');
		this.bind('KeyDown', this.keyDown);		
		this.songName = 'bgMusic'		
	},
	
	stopMusic : function(){
		if(Crafty.audio.isPlaying(this.songName)){				
			Crafty.audio.pause(this.songName);
		}
	},

	fadeMusic : function(time1,time2){
		console.log("fading music?");
		this.timeMax = time1;
		this.timeMax2 = time1 + time2;
		this.state = 'fadeMusic';
	},

	everyFrame : function(){
		// console.log(this.state, Crafty.audio.isPlaying('bgMusic'));		
		// console.log('state : ' + this.state + ',    playing bgmusic? ' + Crafty.audio.isPlaying('bgMusic'));
		if(this.state === 'bgMusic'){

			if(!Crafty.audio.isPlaying(this.songName) && !muteMusic){				
				this.bgMusic = Crafty.audio.play(this.songName,0.8,1);				
			}
		
		} else if (this.state === 'fadeMusic'){
			if(Crafty.audio.isPlaying(this.songName)){
				// console.log(this.timeCounter + "    " + ((this.timeMax- this.timeCounter)/this.timeMax)*this.maxVolume);
				this.timeCounter += 1;
				if(this.bgMusic){
					if(this.timeCounter < this.timeMax){
						this.bgMusic.volume = ((this.timeMax- this.timeCounter)/this.timeMax)*this.maxVolume;
					} else if(this.timeCounter === this.timeMax){
						this.bgMusic.pause();
					} else if(this.timeCounter >= this.timeMax2){
						if(!muteMusic){
							this.bgMusic = Crafty.audio.play(this.songName,0.3,1);
						}
						this.timeCounter = 0;
						this.state = 'bgMusic';
						
					}
				} else {
					
					this.state = "bgMusic";
				}
			} else {
				this.state = 'bgMusic';
			}
		} else {
			this.state = 'bgMusic'; 
		}
	},

	playBgMusic : function(){
		
		this.state = 'bgMusic' ;
		if(!Crafty.audio.isPlaying(this.songName)){
			this.bgMusic = Crafty.audio.play(this.songName,0.7,1);
		}
	},
	
	
	keyDown : function(keyEvent){		
		 if (keyEvent.key === Crafty.keys.M) {
			if(muteMusic == false){
				muteMusic = true;
				Crafty.audio.mute();
			} else {
				muteMusic = false;
				Crafty.audio.unmute();
			}
		}		
	},
	
});


