Crafty.c("Square", {
    init: function() {
		this.requires('2D', 'Canvas', 'Mouse');
	},

    _Square: function(owner, col, row, type, collisionType) {
        this.col = col;
		this.row = row;
        this.type = type;
        this.owner = owner;
        this.collisionType = collisionType;
        if (this.collisionType != "empty"){
            this.requires('Collision', 'SquareSolid');
              // Create small hit area below square to detect whether it can start falling
            this.areaBelow = Crafty.e('2D', 'Collision').collision(
                [this.x, this.y + params.sqSize,
                this.x + params.sqSize, this.y + params.sqSize,
                this.x + params.sqSize, this.y + params.sqSize + params.sqFallMargin,
                this.x, this.y + params.sqSize + params.sqFallMargin
            ]);
            this.attach(this.areaBelow);
        }
        this.x = params.sqSize * col;
        this.y = params.sqSize * row;
        this.w = params.sqSize;
        this.h = params.sqSize;
        this.origin('center');

        this.shade = Crafty.e('2D, Canvas, shade').attr({x:this.x, y:this.y, z:params.zLevels['shade'], visible:false});
        this.attach(this.shade);   
        this.z = params.zLevels['rock'];
        this.attach(this.LU = Crafty.e('RockTileCorner')._RockTileCorner('LU',this.col,this.row, this.type).attr({x:this.x, y:this.y}));
        this.attach(this.RU = Crafty.e('RockTileCorner')._RockTileCorner('RU',this.col,this.row, this.type).attr({x:this.x+(params.sqSize / 2), y:this.y}));
        this.attach(this.RB = Crafty.e('RockTileCorner')._RockTileCorner('RB',this.col,this.row, this.type).attr({x:this.x+(params.sqSize / 2), y:this.y+(params.sqSize / 2)}));
        this.attach(this.LB = Crafty.e('RockTileCorner')._RockTileCorner('LB',this.col,this.row, this.type).attr({x:this.x, y:this.y+(params.sqSize / 2)}));
        return this;
    },

    hammer: function() {
        this.owner.removeBlock();
    },

    highlight: function(){
        this.LU.highlight();
        this.RU.highlight();
        this.RB.highlight();
        this.LB.highlight();
    },

    unhighlight: function(){
        this.LU.unhighlight();
        this.RU.unhighlight();
        this.RB.unhighlight();
        this.LB.unhighlight();
    },

    sqBelow: function() {
        let collisions = this.areaBelow.hit('SquareSolid');
        if (collisions != null) {
            // for (idx = 0; idx < collisions.length; ++idx) {
            //     console.log(collisions[idx]);
            // }
            // Just return the first the best collision object.
            return collisions[0].obj;
        }
        return null;
    },

    updateSprite:function() {
    
        // check which surrounding tiles are rock, adjust own sprite.
        var arrayIdxs = [this._x / params.sqSize, this._y / params.sqSize];
        this.neighboringSquares = this._parent.sqs;
        
        // making empty 3x3 tiles for surroundings
        surroundingTiles = new Array(3);
        for(var i = 0 ; i<3; i++){
            surroundingTiles[i] = [0,0,0];
        } 

        neighborColRows = [];
        for (var k = 0; k<this.neighboringSquares.length; k++){
            let neighbor = this.neighboringSquares[k];
            
            neighborColRows.push([neighbor.col, neighbor.row])
            var dx = neighbor.col - this.col;
            var dy = neighbor.row - this.row;
            
            if (Math.abs(dx) <= 1 && Math.abs(dy) <= 1){

                surroundingTiles[dx + 1][dy + 1] = 1;
            };
        }
        
        if (surroundingTiles[1][1] === 0){ // if this is an air tile
          var dx = (this.type - 1)*3;
          if(surroundingTiles[0][1]+surroundingTiles[1][0] === 2){
            this.LU.sprite(0 + dx,7);        
            this.LU.highlightCorner.sprite(0,7);   
          }else{ this.LU.sprite(0,16);this.LU.highlightCorner.sprite(0,16);}
          if(surroundingTiles[1][0]+surroundingTiles[2][1] === 2){
            this.RU.sprite(0 + dx,7);
            this.RU.highlightCorner.sprite(0,7);   
          }else{ this.RU.sprite(0,16);this.RU.highlightCorner.sprite(0,16);}
          if(surroundingTiles[2][1]+surroundingTiles[1][2] === 2){
            this.RB.sprite(0 + dx,7);
            this.RB.highlightCorner.sprite(0,7);   
          }else{ this.RB.sprite(0,16);this.RB.highlightCorner.sprite(0,16);}
          if(surroundingTiles[1][2]+surroundingTiles[0][1] === 2){
            this.LB.sprite(0 + dx,7);
            this.LB.highlightCorner.sprite(0,7);   
            
          }else{ this.LB.sprite(0,16);this.LB.highlightCorner.sprite(0,16);   }
        }
        else{ // if this tile is a solid tile
            // check shading, if no rock to the right, shade!
            if(surroundingTiles[2][1] === 0 && (this.type == 1 || this.type == 4)) {
                this.shade.visible = true
            }
            var arrayLU = [ surroundingTiles[0][1], surroundingTiles[0][0], surroundingTiles[1][0] ];
            var arrayRU = [ surroundingTiles[1][0], surroundingTiles[2][0], surroundingTiles[2][1] ];
            var arrayRB = [ surroundingTiles[2][1], surroundingTiles[2][2], surroundingTiles[1][2] ];
            var arrayLB = [ surroundingTiles[1][2], surroundingTiles[0][2], surroundingTiles[0][1] ];
            this.LU.updateSprite(arrayLU);
            this.RU.updateSprite(arrayRU);
            this.RB.updateSprite(arrayRB);
            this.LB.updateSprite(arrayLB);
        }
    },

    explode: function(){
        //Particles!            
        e=Crafty.e('2D, Canvas, Particles, Delay')
        .attr({z:params.zLevels['rock']+20,x:this.x + params.sqSize/2-200, y :this.y + params.sqSize/2-200, w:400, h:400})
        // .addComponent('WiredMBR')
        .particles(params.particleOptionsOutputter(this.type))
        .delay(function(){this.destroy()},500,0);
        // this.attach(e);

    },
});

Crafty.c("RockTileCorner",{
    init: function(){
        this.requires('2D, Canvas, tile2'); 
        
        this.h = params.sqSize/2;
        this.w = params.sqSize/2;
        this.origin('center');
    },

    _RockTileCorner: function(cornerType, col, row, type){
        
        this.col = col;
        this.row = row;
        this.cornerType = cornerType;
        this.type = type;
        this.highlightCorner = Crafty.e('2D, Canvas, highlight').attr({z: params.zLevels['rock'] + 20, alpha: 0.5, visible:false}); 
        this.attach(this.highlightCorner);
        this.alpha = this.type==5? 0.7: 1;
        this.z = this.type == 5? params.zLevels["glassRock"] : params.zLevels['rock'] ;
        this.sprite(1,0);
        switch(this.cornerType){
        case 'LU':            
            this.rotation = -90;
            break;
        case 'RU':            
            break;
        case 'RB':            
            this.rotation = 90;
            break;
        case 'LB':            
            this.rotation = 180;
            break;
        };
        return  this;
    },

    highlight: function(){
        this.highlightCorner.visible = true;
    },

    unhighlight: function(){
        this.highlightCorner.visible = false;
    },

    updateSprite: function(array){
        var dx = (this.type - 1) * 3;
        if (utility.compareArrays(array, [0,0,0])){
            this.sprite(1 + dx, 0);
            this.highlightCorner.sprite(1,0);
        } else if (utility.compareArrays(array, [0,0,1])){
            this.sprite(0 + dx, 0);   
            this.highlightCorner.sprite(0,0);
        } else if (utility.compareArrays(array, [0,1,0])){
            this.sprite(0 + dx, 5);   
            this.highlightCorner.sprite(0,5);
        } else if (utility.compareArrays(array, [0,1,1])){
            this.sprite(0 + dx, 3);
            this.highlightCorner.sprite(0,3);   
        } else if (utility.compareArrays(array, [1,0,0])){
            this.sprite(1 + dx, 1);   
            this.highlightCorner.sprite(1,1);
        } else if (utility.compareArrays(array, [1,1,0])){
            this.sprite(1 + dx, 3);   
            this.highlightCorner.sprite(1,3);
        } else if (utility.compareArrays(array, [1,0,1])){
            this.sprite(0 + dx, 1);   
            this.highlightCorner.sprite(0,1);
        } else if (utility.compareArrays(array, [1,1,1])){
            this.sprite(0 + dx, 1);   
            this.highlightCorner.sprite(0,1);
        }     


    }
});
