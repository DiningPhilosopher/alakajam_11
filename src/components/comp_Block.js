Crafty.c("Block", {
	init: function() {
		this.requires("2D, Canvas, Collision, Delay");
		this.x = 0
		this.y = 0
		this.sqs = []
		this.vy = 0.01;
		this.falling = true;
	},

	_Block: function(col, row, shape, tileIdx){
		// shape is an array of two-element arrays with row/col offsets containing at least [0, 0].
		// for example shape = [[0, 0], [0, 1]] means one sq at (row, col) and one at ()
		this.col = col;
		this.row = row;
		this.type = this.tileIdxToBlockType(tileIdx);
		// Do block fall?
		if (this.type === 1 || this.type === 3) {
			this.fixed = true;
		} else {
			this.fixed = false;
		}
		// Can block be destroyed?
		if (params.destroyableTypes.includes(this.type)) {
			this.choppable = true;
		} else {
			this.choppable = false;
		}
		// Can we walk through this block?
		if (this.type == 5) {
			this.walkable = true;
		}

		this.sqs = [];
		this.sqsEmpty = [];
		this.sqsBottom = [];

		this.vy = 0.01;
		this.falling = true;

		let maxCol = 0;
		let maxRow = 0;
		let minCol = 0;
		let minRow = 0;
		let sqsBottomDict = {}
		for (var idx = 0; idx < shape.length; idx++) {
			let sq = Crafty.e('Square')._Square(this, shape[idx][0], shape[idx][1], this.type, "solid");
			if (this.choppable) {
				sq.addComponent("Choppable");
			}
			if (!this.walkable) {
				sq.addComponent("Obstacle")
			}
			this.sqs.push(sq);
			this.attach(sq);
			if (sq.col in sqsBottomDict){				
				sqsBottomDict[sq.col] = Math.max(sqsBottomDict[sq.col],sq.row);
			} else {
				sqsBottomDict[sq.col] = sq.row;
			}
			maxCol = Math.max(maxCol, sq.col);
			maxRow = Math.max(maxRow, sq.row);		
			minCol = Math.min(minCol, sq.col);
			minRow = Math.min(minRow, sq.row);	



		}
		for ( var idx = 0; idx < this.sqs.length; idx++){
			let sq = this.sqs[idx];
			if (sqsBottomDict[sq.col] == sq.row){
				this.sqsBottom.push(sq);
			}
		}

		
		for (var colIdx = minCol; colIdx <= maxCol; colIdx++){
			for (var rowIdx = minRow; rowIdx <= maxRow; rowIdx++){
				var squarePresent = false;
				for (var sqIdx = 0; sqIdx < this.sqs.length; sqIdx++){
					let sq = this.sqs[sqIdx];					
					if (utility.compareArrays([sq.col, sq.row],[colIdx, rowIdx])){
						squarePresent = true;								
					} 		
				}
				
				if (!squarePresent){
					let sq = Crafty.e('Square')._Square(this, colIdx, rowIdx, this.type, "empty"); // -1 = "air"
					this.sqsEmpty.push(sq);
					this.attach(sq);
				}
			}
		}
		// fix the sprites of the squares
		for (var k = 0; k<this.sqs.length; k++){
			this.sqs[k].updateSprite();
		}
		for (var k = 0; k<this.sqsEmpty.length; k++){
			this.sqsEmpty[k].updateSprite();
		}

		this.x = col * params.sqSize;
		this.y = row * params.sqSize;
		return this;
	},

	updateIsFalling: function(){		
		let supported = false;
		// for (let idx = 0; idx < this.sqs.length; ++idx) {
		// 	let sq = this.sqs[idx];
		for (let idx = 0; idx < this.sqsBottom.length; ++idx) {
			let sq = this.sqsBottom[idx];
			let sqBelow = sq.sqBelow();
			if (sqBelow != null && sqBelow.owner != sq.owner) {
				// found a support
				this.falling = false;
				return;
			}
		}
		// Did not find any supports, this block is falling.
		this.falling = true;
	},

	snapToGrid: function() {
		this.y = Math.round(this.y / params.sqSize) * params.sqSize;
	},

	updatePos: function(dt) {
		if (!this.fixed){			
			this.updateIsFalling();
			if(this.falling) {
				this.vy = Math.min(params.maxVy, this.vy + params.gravity * dt);
				this.y += this.vy * dt;
		    } else {
				this.vy = 0;
				this.snapToGrid();
			}
		}
	},

	removeSquare: function(square) {
		// for (idx = 0; idx < this.sqs.length; ++idx) {
		// 	let sq = this.sqs[idx];
		// 	if (sq.col == square.col && sq[1] == square[1]) {

		// 	}
		// }
		let idx = this.sqs.indexOf(square);
		if (idx == -1) {
			console.log("WARNING: trying to remove square from block that does not contain it.");
		} else {
			this.sqs.splice(idx, 1);
		}
	},

	tileIdxToBlockType: function(tileIdx) {
		if (tileIdx == 0) {
			return 0;
		} else if (tileIdx <= 5) {
			return 1;
		} else if (tileIdx <= 10) {
			return 2;
		} else if (tileIdx <= 15) {
			return 3;
		} else if (tileIdx <= 20) {
			return 4;
		} else if (tileIdx <= 25) {
			return 5;
		}
	},

	highlight: function(){
     	for (var i=0; i<this.sqs.length;i++){
     		this.sqs[i].highlight();
     	}

     	for (var i=0; i<this.sqsEmpty.length;i++){
     		this.sqsEmpty[i].highlight();
     	}
    },

    unhighlight: function(){
     	for (var i=0; i<this.sqs.length;i++){
     		this.sqs[i].unhighlight();
     	}

     	for (var i = 0; i < this.sqsEmpty.length; i++){
     		this.sqsEmpty[i].unhighlight();
     	}
    },

    removeBlock: function(){
    	for (var i=0; i<this.sqs.length;i++){
     		this.sqs[i].explode();
     	}
     	// Math.floor(Math.random()*6).toString();
    	Crafty.audio.play("crumble"+Math.floor(Math.random()*6).toString(),0.4,1);
    	// this.delay(function(){
    		this.destroy();
    	// },300);
    },
});