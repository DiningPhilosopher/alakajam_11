Crafty.c("Axe", {
    init: function() {
        this.requires("2D, Canvas, Collision");
        this.axeW = 14;
        this.axeH = 14;
        this.centerX = -0.5 * this.axeW;
        this.offsetX = this.centerX;
        this.block = null;
    },

    _Axe: function() {
        this.w = this.axeW;
        this.h = this.axeH;
        return this;
    },

    chop: function(dir) {
        this.chopping = true;
    },

    stopChop: function() {
        this.chopping = false;
    },

    updatePos: function(dir) {
        this.offsetX = this.centerX + dir * params.chopMax
        this.x = this._parent.ox + this.offsetX;
        this.y = this._parent.oy - 15;
        let collisions = this.hit('Choppable');
        if (collisions == null && this.block != null) {
            this.block.unhighlight();
        }
        if (collisions != null) {
            let newBlock = collisions[0].obj.owner;
            if (this.block != null && this.block != newBlock) {
                this.block.unhighlight();
            }
            this.block = newBlock;
            this.block.highlight();
            if (this.chopping) {
                this.block.removeBlock();
                Crafty.trigger("crushStone");
                this.block = null;
                this.stopChop();
            }
        }
    },
})