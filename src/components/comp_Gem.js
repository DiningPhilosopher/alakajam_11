Crafty.c("Gem", {
    init: function() {
        this.requires("2D, Canvas, Collision, gem, Tween, Delay");
        let min = 5;
        let max = params.sqSize - min;
        this.collision([min, min, max, min, max, max, min, max])
        this.origin("center");
    },

    collect: function(){
    	
    	this.tween({x:scoreUI.gemScorePic.x+params.sqSize, y:scoreUI.gemScorePic.y+params.sqSize, rotation:720},1000,"easeOutQuad");
    	this.removeComponent("Gem");
    	this.delay(function(){this.destroy()},1000);
    },
})