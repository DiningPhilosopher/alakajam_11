Crafty.c("ScoreUI", {
	init: function() {
		this.requires("2D, UI, Color, Delay"); 		
		this.h = 100;//UIheight;
		this.w = Game.width;
				
		this.fadeScreen = Crafty.e("2D, UI, Color, Tween").color(mycolors.fadeScreen).attr({w:Game.width, h:Game.height,z:params.zLevels["fadeScreen"]});		
		this.oldLookScreen = Crafty.e("2D, UI, oldLook").attr({z:params.zLevels["oldLook"], alpha:0.3});	
		this.gemScorePic = Crafty.e("2D,UI,gem").attr({x:20,y:25,alpha:1});
		this.gemScoreDisplay = Crafty.e('2D,UI,Text,Tween')
		.attr({x:65, y:20, w:800, h:60, alpha:1,z:30}).textColor(mycolors.infoText)		
		.textFont({ family: fontFamily2,size: '39px'}).textAlign('left').text("");
	
		// this.totalScoreDisplay = Crafty.e('2D,UI,Text,Tween')		
		// .attr({x:20, y:80, w:800, h:60, alpha:0,z:30}).textColor(mycolors.infoText)		
		// .textFont({ family: fontFamily2,size: '30px'}).textAlign('left').text("Total: 0");
	
		// this.winText = Crafty.e('2D,UI,Text,Tween')		
		// .attr({x:0, y:360, w:Game.width, h:200, alpha:0,z:30}).textColor(mycolors.infoText)		
		// .textFont({ family: fontFamily2,size: '50px'}).textAlign('center').text(" ");
		
		// this.showTotal = true;
		// this.bestTotal = -1;
		
		// this.musicButton = Crafty.e('2D, UI, music, Mouse').attr({alpha:1, x: 478, y: 100, z:13})
		// .bind('MouseDown',function(){
			// if(muteMusic){
				// muteMusic = false;
				// bgMusic.playBgMusic();
				// this.sprite(0,0);
			// } else {
				// muteMusic =  true;
				// bgMusic.stopMusic();
				// this.sprite(1,0);			
			// }
		// });
		// if(muteMusic){this.musicButton.sprite(1,0);}
		// this.soundButton = Crafty.e('2D, UI, sound, Mouse').attr({alpha:1, x: 578-60, y: 0, z:13})
		// .bind('MouseDown',function(){
			// if(muteAudio){
				// muteAudio = false;
				// this.sprite(0,0);				
			// } else {
				// muteAudio =  true;	
				// this.sprite(1,0);			
			// }
		// });
		// if(muteAudio){this.soundButton.sprite(1,0);}


	},
	
	_ScoreUI : function(nrOfLevels){ 	
		this.highscores = [];
		this.scores = [];
		// this.score = 0

		for( var i =0 ; i< nrOfLevels; i++){
			this.highscores.push(-1); 
			this.scores.push(-1); 
		}
		checkForHighscores();
		if(highscores){ // als nog opgeslagen			
			for( var j =0; j< highscores.length; j++){								
				this.highscores[j] = highscores[j]>0? highscores[j] : -1;				
			}
		}	 		
		
		
		return this;
	},

	

	fadeIn: function(time){
		this.fadeScreen.tween({alpha:0},time);
	},

	fadeOut: function(time){
		this.fadeScreen.tween({alpha:1},time);
	},

	

	updateGemScore : function(gems,gemsGoal){
		this.gemScoreDisplay.alpha = 1;
		this.gemScoreDisplay.text( ": "+gems.toString() + "/" + gemsGoal.toString());			
		
	},

	
		
});


