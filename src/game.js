
let muteMusic = false;
let muteAudio = false;
let debug = false;//true;
let playing = false;
let highscores = 0;
let fontFamily1 = "BioRhyme Expanded"; // title
let fontFamily2 = "Indie Flower"; // info text
let UIheight = 0;
let UIwidth = 0;
let bgMusic = null;
// goal.type == 'timed' OR 'points'

function saveState(state) {
	// console.log("Saving scores: ", JSON.stringify(state));
	window.localStorage.setItem("gameState", JSON.stringify(state));
	
};
		 
function restoreState() {
	var state = window.localStorage.getItem("gameState");	
	if (state) {
		return JSON.parse(state);
	} else {
		return null;
	}
	
};

function checkForHighscores(){
	var dummyvar = restoreState();	
	if(dummyvar){			
		if(dummyvar["highscores_alakajam_11"]){		
			console.log("Found existing highscores! ", dummyvar["highscores_alakajam_11"]);
			highscores = dummyvar["highscores_alakajam_11"];
		} else {
			console.log("No highscores found.");
			highscores = null;		
		}	
	}
}

// checkForHighscores(); // happens in scoreUI

Crafty.paths({
	audio: 'assets/sound/',
	images: 'assets/images/'
});

Game = {
	width: params.viewportW + UIwidth*2,
	height: params.viewportH + UIheight,
	crystalCounter: 0,
	tportCooldown: 0,    
	start: function() {
		// Start crafty and set a background color so that we can see it's working
		Crafty.init(Game.width,Game.height, 'cr-stage');
		Crafty.viewport.clampToEntities = false;
		Crafty.scene('Loading');
	},
};