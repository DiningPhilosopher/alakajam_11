const params = {
    viewportW: 1200,
    viewportH: 880,

    // DO NOT CHANGE PARAMETERS
    // The puzzles depend on them.
    gravity: 0.001,
    maxVy: 2.0,

	sqSize: 40,
    sqFallMargin: 10,

    chopSpeed: 0.2,
    chopMax: 15,
    
    destroyableTypes: [2, 3, 5],

    zLevels: {        
        rock: 1050,
        shade: 1070,
        oldLook: 1200,
        player: 3000,
        glassRock: 4000,
        explanations: 4500,
        fadeScreen: 5000,
    },

    particleOptionsOutputter: function(type){
    	switch(type){
			case 2:
				color = [107, 124, 134, 1];
				break;
			case 3:
				color = [151, 122, 87, 1];
				break;
			case 5:
				color = [144, 220, 220, 1];
				break;
			default:
				color = [124, 122, 114, 1];
				break;
	    };
	    return {
		    maxParticles: 4,
		    size: 12,
		    sizeRandom: 7,
		    speed: 3,
		    speedRandom: 5,
		    // Lifespan in frames
		    lifeSpan: 20,
		    lifeSpanRandom: 2,
		    // Angle is calculated clockwise: 12pm is 0deg, 3pm is 90deg etc.
		    angle:0,
		    angleRandom: 45,
		    startColour: color,
		    startColourRandom: [5, 5, 5, 0],
		    endColour: color,
		    endColourRandom: [5, 5, 5, 0],
		    // Only applies when fastMode is off, specifies how sharp the gradients are drawn
		    sharpness: 50,
		    sharpnessRandom: 0,
		    // Random spread from origin
		    spread: 10,
		    // How many frames should this last
		    duration: -1,//30,
		    // Will draw squares instead of circle gradients
		    fastMode: true,
		    gravity: { x: 0, y: 0.4 },
		    // sensible values are 0-3
		    jitter: 0.2,
		    // Offset for the origin of the particles
		    originOffset: {x:200, y: 200}
	  	};
  	},
}