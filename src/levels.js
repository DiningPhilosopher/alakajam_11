levels = ["tutorial1", "tutorial2","tutorial3", "tutorialDragon", "traffic", "high_and_low", "tower", "labyrinth", "endOfGame"];

explanations = {
	"tutorial1" : [{x:300, y:100, text: "Welcome to Ruins & Riches!<br>You must procure all the gems hidden<br>in these ancient ruins, but minimize<br>the damage you do to this<br>invaluable cultural heritage."}],
	"tutorial2" : [{x:120, y:400, text: "Some stones can be demolished.<br>Use SPACE near such stones."},
				   {x:700, y:300, text: "Be careful, don't get crushed!"}],
	"tutorial3" : [{x:420, y:600, text: "Press R to restart."}]
}