import numpy as np


text_file = open("level.txt", "r")
input = text_file.read().split('\n')
text_file.close()
pointList = []
minY=30000
for rawpoint in input:
    point = [ int(float(i)) for i in rawpoint.split('\t')]
    pointList.append(point)
    minY = min(minY,point[1])
print(pointList)
print(minY)





pointsListText = "["
for pointIdx in range(len(pointList)):
    point = pointList[pointIdx]

    pointText  = "["+  str(point[0]) + "," + str(point[1]-minY) + "]"
    pointsListText += pointText
    if pointIdx < len(pointList)-1:
        pointsListText += ","
pointsListText += "]"
print(pointsListText)

test = '''`search_data` funnelData.funnelName = \"Subscription recommender\" eventData.experimentData = \"*
 *\" | fields eventData.experimentData userData.userID debugData.userAgentData.browser.name debugData.userAgentData.device.type userData.sessionCount userData.hasAdBlock sessionData.sessionID funnelData.funnelStep funnelData.subscriptionType | rename eventData.experimentData AS experimentData userData.userID as userID debugData.userAgentData.browser.name as browser debugData.userAgentData.device.type as deviceType userData.sessionCount as sessionCount userData.hasAdBlock as hasAdblock sessionData.sessionID as sessionID  funnelData.funnelStep as funnelStep funnelData.subscriptionType as subscriptionType     | rex field=experimentData \".* 
 :(?<variant>\\w+)(,.*)?$\" | eval experimentID = \"
 \" | eval returning = if(sessionCount > 1, \"true\", \"false\"), deviceType=lower(deviceType) | eval deviceType = coalesce(deviceType, \"unknown\"), browser = coalesce(browser, \"unknown\"), hasAdblock = coalesce(hasAdblock, \"unknown\") | eval hasAdblock = coalesce(hasAdblock, \"unknown\") | stats latest(hasAdblock) as hasAdblock count as funnel_visit count(eval(funnelStep=\"Purchase\" AND subscriptionType=\"Trial\")) as proefabonnementen count(eval(funnelStep=\"Purchase\" AND subscriptionType=\"Ongoing\")) as vaste_abonnementen  count(eval(funnelStep=\"Purchase\" AND isnull(subscriptionType))) as andere_abonnementen by userID, variant, browser, deviceType, returning, experimentID | stats dc(userID) as users sum(proefabonnementen) as proefabonnementen sum(vaste_abonnementen) as vaste_abonnementen sum(andere_abonnementen) as andere_abonnementen by variant, browser, deviceType, returning, hasAdblock, experimentID '''

print(test)